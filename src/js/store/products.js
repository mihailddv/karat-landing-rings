import Vue from 'vue'
import Vuex from 'vuex'
import smoothscroll from 'smoothscroll-polyfill';

Vue.use(Vuex)

smoothscroll.polyfill();

const store = new Vuex.Store({
    state: {
        loading: true,
        productsAll: [],
        products: [],
        productsId: [],
        productsCount: 0,
        pages: 0,
        startPage: 1,
        currentPage: 1,
        currentHashPage: 1,
        limit: 6,
        offset: {
            start: 0,
            end: 0
        }
    },
    mutations: {
        GET_CURRENT_HASH(state) {
            let page = window.location.hash.slice(6)

            if (isNaN(page) || page < 1) {
                page = 1
            }

            state.currentHashPage = page
            state.startPage = page
        },
        SET_CURRENT_HASH(state) {
            if (state.currentHashPage != state.currentPage) {
                state.currentHashPage = state.currentPage

                window.location.hash = 'page-' + state.currentPage
            }
        },
        FETCH_PRODUCTS(state, products) {
            state.productsAll = products

            state.productsCount = products.length

            state.pages = Math.ceil(state.productsCount / state.limit)

            state.loading = false
        },
        SET_CURRENT_PAGE(state, page) {
            page = parseInt(page)

            if (isNaN(page) || page < 1) {
                page = 1
            }

            if (state.pages > 0 && page > state.pages) {
                page = state.pages
            }

            state.currentPage = page
        },
        PRE_PAGE(state) {
            if (state.currentPage > 1) {
                state.currentPage -= 1
            }
        },
        NEXT_PAGE(state) {
            if (state.currentPage < state.pages) {
                state.currentPage += 1
            }
        },
        RENDER_PRODUCTS(state, clear) {
            if (state.currentPage > 1) {
                state.offset.start = (state.currentPage - 1) * state.limit;
                state.offset.end = state.offset.start + state.limit
            } else {
                state.offset.start = 0
                state.offset.end = state.limit
            }

            let products = state.productsAll.slice(state.offset.start, state.offset.end)

            let id = false

            if (state.productsId.length) {
                id = state.productsId.includes(products[0].id)
            }

            console.log('id', id)
            console.log('startPage', state.startPage)
            console.log('currentPage', state.currentPage)
            console.log('productId', products[0].id)

            if (id) {
                document.getElementById('item_id_' + products[0].id).scrollIntoView({ block: 'start', behavior: 'smooth' });
            } else {
                if (state.currentPage < state.startPage) {
                    state.products = []
                    state.productsId = []
                }

                products.forEach(product => state.products.push(product))
                products.forEach(product => state.productsId.push(product.id))

                if (state.currentPage < state.startPage) {
                    setTimeout(function () {
                        document.getElementById('item_id_' + products[0].id).scrollIntoView({ block: 'start', behavior: 'smooth' })
                    }, 150)
                }
            }
        }
    },
    actions: {
        loadProducts({ dispatch, commit, getters }) {
            console.log('loadProducts')

            commit("GET_CURRENT_HASH")
            commit("SET_CURRENT_PAGE", getters.getHashPage)

            try {
                fetch('/doruna/data/products.json').then((response) => {
                    response.json().then((json) => {
                        commit("FETCH_PRODUCTS", json)
                        commit("RENDER_PRODUCTS")
                    });
                });
            } catch (error) {
                console.log(error)
            }
        },
        renderProducts({ dispatch, commit }) {
            commit("RENDER_PRODUCTS")
        },
        setPage({ commit }, page) {
            commit("SET_CURRENT_PAGE", page)
            commit("SET_CURRENT_HASH")
            commit("RENDER_PRODUCTS", true)

        },
        prePage({ commit, getters }) {
            if (getters.isPre) {
                commit("PRE_PAGE")
                commit("RENDER_PRODUCTS")
                commit("SET_CURRENT_HASH")
            }
        },
        nextPage({ commit, getters }) {
            if (getters.isNext) {
                commit("NEXT_PAGE")
                commit("RENDER_PRODUCTS")
                commit("SET_CURRENT_HASH")
            }
        }
    },
    getters: {
        getProducts: state => {
            return state.products
        },
        isLoading: state => {
            return state.loading
        },
        isPre: state => {
            return (state.currentPage == 1) ? false : true;
        },
        isNext: state => {
            return (state.currentPage == state.pages) ? false : true;
        },
        getPages: state => {
            return state.pages
        },
        getCurrentPage: state => {
            return state.currentPage
        },
        getHashPage: state => {
            return state.currentHashPage
        }
    }
})

export default store