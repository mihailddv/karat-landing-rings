import lazyLoad from './components/lazyLoad';
// import advantagesVue from './parts/advantages';
import filtersVue from './parts/filters';
import productsVue from './parts/products';
import animate from './components/animate';

require('es6-promise').polyfill();
require('isomorphic-fetch');
require('nodelist-foreach-polyfill');

// vue components
// advantagesVue();
filtersVue();
productsVue();

// componentes
lazyLoad();
animate();
