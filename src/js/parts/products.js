import Vue from 'vue'
import store from '../store/products'
import Products from '../../components/Products.vue'

export default function productsVue() {
    new Vue({
        el: '#products',
        store,
        render: (h) => h(Products),
    })
}