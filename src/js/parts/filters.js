import Vue from 'vue';
import Filters from '../../components/Filters.vue';

export default function advantagesVue() {
  new Vue({
    el: '#filters',
    render: (h) => h(Filters),
  });
}
