import Vue from 'vue';
import Advantages from '../../components/Advantages.vue';

export default function advantagesVue() {
  new Vue({
    el: '#advantages',
    render: (h) => h(Advantages),
  });
}
