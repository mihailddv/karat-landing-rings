const WOW = require('wowjs');

export default function animate() {
  window.wow = new WOW.WOW({
    live: false,
  });

  window.wow.init();
}
