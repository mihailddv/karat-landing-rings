import LazyLoad from 'vanilla-lazyload';

export default function lazyLoad() {
  new LazyLoad({
    elements_selector: '.lazy',
    // ... more custom settings?
  });
}
